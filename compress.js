const Kafka = require('node-rdkafka');

const brokers = process.env.BROKER_LIST || "localhost:9092";
const idCliente = process.env.CLIENT_ID || "kafkatrain";
const topico  = process.env.TOPICO || "meu.topico";
const tamanhoRegistro = parseInt(process.argv[2] || 1000);
const tamanhoLote = process.argv[3] || 1000 * 2;
const quantidadeRegistro = process.argv[4] || 1;
const chave = process.argv[5] || null;
const carimbar = process.argv[6] || "sim";

console.log("  Brokers...: ", brokers);
console.log("  ID Cliente: ", idCliente);
console.log("  Topico....: ", topico);
console.log("    Tamanho do Registro........: ", tamanhoRegistro);
console.log("    Tamanho do Lote............: ", tamanhoLote);
console.log("    Quantidade de Registros....: ", quantidadeRegistro);
console.log("    Tamanho da Mensagem (batch): ", tamanhoRegistro * quantidadeRegistro);
console.log("    Chave......................: ", chave);
console.log("    Carimbar...................: ", carimbar);

const producer = new Kafka.Producer({
  "metadata.broker.list" : brokers,
  "client.id"            : idCliente,
  "batch.num.messages"   : tamanhoLote,
  "compression.type"     : "lz4",
  "dr_cb"                : true
});

/*
 * batch.num.messages
 *   equivalente a configuração batch.size
 * 
 * dr_cb
 *   é uma configuração específica do driver, 
 *   que significa: Delivery Report CallBack
 */

// Nenhuma partição específica
const qualquerParticao = null;

// CreateTime
let timestamp = Date.now();
if("nao" === carimbar){
  timestamp = null;
  console.log("Não carimbar data/hora");
}

// Conectar ao broker
producer.connect();

// Corpo do registro
let corpoRegistro = Buffer.alloc(tamanhoRegistro);
corpoRegistro.fill("K");

// Quando estiver pronto, podemos produzir registros
producer.on("ready", () => {
  console.log("\n  Teclar CTRL+C para finalizar . . .");
  try{
    for(let i = 0; i < quantidadeRegistro; i++) {
      producer.produce(
        topico,
        qualquerParticao,
        corpoRegistro,
        chave,
        timestamp
      );
    }
  }catch(err) {
    console.error("Erro ao produzir registro:");
    console.error(err);
  }
});

// Relatórios de entrega
producer.on("delivery-report", (err, report) => {
  if(err){
    console.error(err);
  } else {
    console.log("\nO registro foi produzido: \n", JSON.stringify(report));
  }
});

producer.on("error", (err) => {
  console.error("Erros no producer: " + err);
});

producer.setPollInterval(100);
