const Kafka = require('node-rdkafka');

const brokers = process.env.BROKER_LIST || "localhost:9092";
const idCliente = process.env.CLIENT_ID || "kafkatrain";
const topico  = process.env.TOPICO || "meu.topico";
const mensagem = process.argv[2] || "Uma mensagem qualquer";
const chave = process.argv[3] || null;
const carimbar = process.argv[4] || "sim";

console.log("  Brokers...: ", brokers);
console.log("  ID Cliente: ", idCliente);
console.log("  Topico....: ", topico);
console.log("    Mensagem: ", mensagem);
console.log("    Chave...: ", chave);
console.log("    Carimbar: ", carimbar);

const producer = new Kafka.Producer({
  "metadata.broker.list" : brokers,
  "client.id"            : idCliente,
  "dr_cb"                : true
});

/*
 * dr_cb
 *  é uma configuração específica do driver, 
 *  que significa: Delivery Report CallBack
 */

// Nenhuma partição específica
const qualquerParticao = null;

// CreateTime
let timestamp = Date.now();
if("nao" === carimbar){
  timestamp = null;
  console.log("Não carimbar data/hora");
}

// Conectar ao broker
producer.connect();

// Quando estiver pronto, podemos produzir registros
producer.on("ready", () => {
  console.log("\n  Teclar CTRL+C para finalizar . . .");
  try{
    producer.produce(
      topico,
      qualquerParticao,
      Buffer.from(mensagem),
      chave,
      timestamp
    );
  }catch(err) {
    console.error("Erro ao produzir mensagem:");
    console.error(err);
  }
});

// Relatórios de entrega
producer.on("delivery-report", (err, report) => {
  if(err){
    console.error(err);
  } else {
    console.log("\nA mensagem foi produzida: \n", JSON.stringify(report));
  }
});

producer.on("error", (err) => {
  console.error("Erros no producer: " + err);
});

producer.setPollInterval(100);
