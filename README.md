# Producer escrito em Node

## KafkaJS

```bash
export OAUTH_CLIENT_ID='<client_id>'
export OAUTH_CLIENT_SECRET='<client_secret>'
export OAUTH_TOKEN_ENDPOINT_URI='<endpoint>'

BROKER_LIST='<host:porta>' \
NODE_TLS_REJECT_UNAUTHORIZED=0 \
GROUP_ID='<meu-grupo>' \
node producer-kafkajs.js '<meu-topico>'
```