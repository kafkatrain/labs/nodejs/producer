const { Kafka } = require('kafkajs');
const axios = require('axios');
const qs = require('qs');

const brokers = process.env.BROKER_LIST || "localhost:9092";
const topico = process.argv[2] || "meu.topico";

const client_id = process.env.OAUTH_CLIENT_ID;
const client_secret = process.env.OAUTH_CLIENT_SECRET;
const token_endpoint = process.env.OAUTH_TOKEN_ENDPOINT_URI || 'https://keycloak.keycloak:30443/auth/realms/master/protocol/openid-connect/token';

console.log("Brokers.........: ", brokers);
console.log("Topico..........: ", topico);

const get_token = {
  client_id: client_id,
  client_secret: client_secret,
  scope: 'email',
  grant_type: 'client_credentials'
};

const axios_config = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
}

const kafka = new Kafka({
  clientId: 'app.example',
  brokers: [brokers],
  // authenticationTimeout: 1000,
  // reauthenticationThreshold: 10000,
  ssl: true,
  sasl: {
    mechanism: 'oauthbearer',
    oauthBearerProvider: async () => {

      const token = await axios.post(token_endpoint, qs.stringify(get_token), axios_config);

      return {
        value: token.data.access_token
      }
    }
  },
});

async function run(){
    const producer = kafka.producer();

    await producer.connect()
    await producer.send({
        topic: topico,
        messages: [
            { key: 'key1', value: 'hello world' },
            { key: 'key2', value: 'hey hey!' }
        ],
    });

    await producer.disconnect();
}

run()
    .then((result) => {})
    .catch(console.error)